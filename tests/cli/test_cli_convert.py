##############################################################################
# Copyright (C) 2020 - 2021 Tobias Röttger <dev@roettger-it.de>
#
# This file is part of F4RATK.
#
# F4RATK is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##############################################################################

from pathlib import Path
from shutil import copy
from typing import Generator

from _pytest.pytester import LineMatcher
from click.testing import CliRunner
from pytest import fixture

from f4ratk.cli.commands import main


class TestConvertCommand:
    @fixture(scope='class', name='resources')
    def resources(self) -> Path:
        test_base = next(p for p in Path(__file__).parents if p.name == 'tests')
        resources = test_base.joinpath('resources')

        assert resources.exists()
        assert resources.is_dir()

        yield resources

    def given_a_msci_index_returns_file_when_convert_should_write_content_as_csv_as_sibling_in_same_directory(  # noqa: E501
        self, tmp_path: Path, resources: Path
    ):
        def given():
            testfile = resources.joinpath('msci-index.xls')
            copy(testfile, tmp_path)

        given()

        assert (
            CliRunner()
            .invoke(main, ('convert', 'MSCI', str(tmp_path.joinpath('msci-index.xls'))))
            .exit_code
            == 0
        )

        converted = tmp_path.joinpath('msci-index.csv')
        assert converted.exists()

        with open(converted, 'r') as handle:
            content = handle.read().splitlines()

        assert len(content) == 12
        assert content[0] == '2001-01,325.325'
        assert content[5] == '2001-06,1005.21'
        assert content[11] == '2001-12,200.25'


class TestConvertCommandHelp:
    @fixture(scope='class', name='output')
    def given_help_option_when_invoked(self) -> Generator[str, None, None]:
        result = CliRunner().invoke(main, ('convert', '--help'))
        assert result.exit_code == 0
        yield result.output

    def should_display_help_option(self, output: str):
        assert '--help' in output

    def should_display_source_type_argument(self, output: str):
        LineMatcher(output.splitlines()).re_match_lines(
            r"Usage\:.*?convert.*?" + "MSCI"
        )

    def should_display_source_path_argument(self, output: str):
        LineMatcher(output.splitlines()).re_match_lines(
            r"Usage\:.*?convert.*?" + "SOURCE"
        )
